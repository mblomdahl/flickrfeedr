'use strict';

/**
 * @author Mats Blomdahl <mats.blomdahl@gmail.com>
 * @version 0.1
 * @description
 *
 * The `FlickrFeedFactory` contain list items retrieved from the [Flickr Public Feed JSON API][1]. API output example:
 *
 *     jsonFlickrFeed({
 *         title: "Uploads from everyone",
 *         link: "http://www.flickr.com/photos/",
 *         description: "",
 *         modified: "2013-10-06T21:21:19Z",
 *         generator: "http://www.flickr.com/",
 *         items: [
 *             {
 *                 title: "Different Bridges",
 *                 link: "http://www.flickr.com/photos/40950306@N08/10125594754/",
 *                 media: {
 *                     m: "http://farm6.staticflickr.com/5538/10125594754_26f6bd1250_m.jpg"
 *                 },
 *                 date_taken: "2013-10-03T12:20:28-08:00",
 *                 description: " <p><a href="http://www.flickr.com/people/40950306@N08/">ChristineW49</a> posted a photo:</p> <p><a href="http://www.flickr.com/photos/40950306@N08/10125594754/" title="Different Bridges"><img src="http://farm6.staticflickr.com/5538/10125594754_26f6bd1250_m.jpg" width="156" height="240" alt="Different Bridges" /></a></p> <p>A modern bridge with a shallower slope up</p>",
 *                 published: "2013-10-06T21:21:20Z",
 *                 author: "nobody@flickr.com (ChristineW49)",
 *                 author_id: "40950306@N08",
 *                 tags: "venice italy bridges"
 *             },
 *             /.../
 *         ]
 *     });
 *
 * [1]: http://api.flickr.com/services/feeds/photos_public.gne?format=json
 */
flickrFeedrApp.factory('FlickrFeedFactory', function ($resource, AppState) {
    var lastItemsList = [];
    var lastItemsMap = {};
    var queryTags = AppState.tags;

    /**
     * @description
     *
     * Performs clean-up of items read from the Flickr Public Feed JSON API.
     *
     * @param {object} data Flickr API response data
     * @param {object} headersGetter Built-in AngularJS headers object
     * @returns {array} Pre-processed Flickr API items
     */
    function transformFn(data, headersGetter) {
        var itemsMap = {};
        var itemsList = data.items;

        for (var i = itemsList.length; i--;) {

            // Parse dates
            itemsList[i].published = new Date(itemsList[i].published);
            if (itemsList[i].date_taken) {
                itemsList[i].date_taken = new Date(itemsList[i].date_taken);
            }

            // Removes username wrapper, e.g. `nobody@flickr.com (ChristineW49)` -> `ChristineW49`
            var author = /\((.+)\)$/ig.exec(itemsList[i].author);
            if (author) {
                itemsList[i].author = author[author.length - 1];
            }

            // Replaces the API output `media` field with its single member `m` (i.e. the image URL)
            itemsList[i].media = itemsList[i].media.m;

            // Converts tag list to array, e.g. `"venice italy bridges"` -> `['venice', 'italy', 'bridges']`
            itemsList[i].tags = itemsList[i].tags.split(' ');

            // Sets the item `id` property to the `<author><image>` component of the item link
            var itemId = itemsList[i].link.replace('http://www.flickr.com/photos/', '').split('/').join('');

            itemsMap[itemId] = itemsList[i];
            itemsList[i].id = itemId;
        }

        lastItemsMap = itemsMap;
        lastItemsList = itemsList;

        return itemsList;
    }

    /**
     * @description
     *
     * Extracts trailing `p` tags with item description.
     *
     * @param {string} description
     * @returns {string} HTML markup of item description `p` tags (if such exists)
     */
    function unwrapDesc(description) {
        var descElement = document.createElement('div');
        descElement.innerHTML = description;
        var pElements = descElement.getElementsByTagName('p');

        // Exclude leading `img` and `author` paragraphs from return value
        var descHtml = [];
        for (var i = 2, j = pElements.length; i < j; i++) {
            descHtml.push(pElements[i].outerHTML);
        }
        return descHtml.join('');
    }

    var resource = $resource(
        '//api.flickr.com/services/feeds/photos_public.gne',
        {
            format: 'json',
            jsoncallback: 'JSON_CALLBACK'
        },
        {
            query: {
                method: 'JSONP',
                params: {
                    tags: function() {
                        return queryTags;
                    }
                },
                transformResponse: transformFn,
                /*timeout: <promise>,
                interceptor: {
                response: <responseHandler>,
                responseError: <responseErrorHandler>
                },*/
                isArray: true
            }
        }
    );

    /**
     * @description
     *
     * Retrieves list of items from the Flickr API based on the `tags` query parameter.
     *
     * @param {string} tags `tags` parameter for the remote API
     * @returns {array} Flickr API items
     */
    this.listByTags = function(tags) {
        tags = tags.split(' ').join(',');
        if (tags == queryTags) {
            return lastItemsList;
        } else {
            AppState.tags = queryTags = tags;
            return resource.query();
        }
    };

    /**
     * @description
     *
     * Retrieves item details by `id` from the most recent API query. Performs clean-up of `description` field.
     *
     * @param {string} id Item ID property
     * @returns {object} Flickr API items
     */
    this.getById = function(id) {
        var item = lastItemsMap[id];
        if (item) {
            item.description = unwrapDesc(item.description);
        }
        return item;
    };

    return this;
});
