'use strict';

var flickrFeedrApp = angular.module('flickrFeedrApp', ['ngResource', 'ngSanitize', 'ngRoute']);

/**
 * @author Mats Blomdahl <mats.blomdahl@gmail.com>
 * @version 0.1
 * @description
 *
 * `$provider` service for storing misc. application state properties.
 */
flickrFeedrApp.value('AppState', {
    tags: ''
});

/**
 * @author Mats Blomdahl <mats.blomdahl@gmail.com>
 * @version 0.1
 * @description
 *
 * Provides routing support. Lacks error handling.
 */
flickrFeedrApp.config(function($routeProvider) {
    $routeProvider.when('/listTags/:tags', {
        templateUrl: 'partials/records-list.html',
        controller: 'RecordsListCtrl'
    });

    $routeProvider.when('/viewRecordDetail/:recordId', {
        templateUrl: 'partials/record-detail.html',
        controller: 'RecordDetailCtrl'
    });

    $routeProvider.otherwise({
        redirectTo: '/listTags/potato'
    });
});
