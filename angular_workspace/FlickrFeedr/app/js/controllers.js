'use strict';

/**
 * @author Mats Blomdahl <mats.blomdahl@gmail.com>
 * @version 0.1
 * @description
 *
 * The `RecordsListCtrl` controller manages view-model of `partials/records-list.html`.
 */
flickrFeedrApp.controller('RecordsListCtrl', function($scope, $routeParams, FlickrFeedFactory) {
    $scope.items = FlickrFeedFactory.listByTags($routeParams.tags);
});

/**
 * @author Mats Blomdahl <mats.blomdahl@gmail.com>
 * @version 0.1
 * @description
 *
 * The `RecordDetailCtrl` controller manages view-model of `partials/record-detail.html`.
 */
flickrFeedrApp.controller('RecordDetailCtrl', function($scope, $window, $routeParams, FlickrFeedFactory) {
    $scope.$back = function() {
        $window.history.back();
    };
    $scope.item = FlickrFeedFactory.getById($routeParams.recordId) || {};
});
