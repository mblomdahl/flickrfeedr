Ext.Loader.setConfig( {
    disableCaching: false,
    enabled       : true
} );

// Fire custom `exTokenChange` event on hash change
(function() {
    Ext.syncRequire('Ext.util.History');
    window.location.hash = '';
    Ext.util.History.init();
    var oldToken = {};
    Ext.History.on('change', function(token) {
        token = token.split('/');
        var newToken = {};
        if (token.length) {
            newToken.token = token.splice(0, 1)[0];
            newToken.argument = token.join('/');
        }
        Ext.globalEvents.fireEvent('exTokenChange', newToken, oldToken);
        oldToken = newToken;
    });
})();

Ext.define('FlickrFeedrD.Application', {
    name: 'FlickrFeedrD',

    extend: 'Ext.app.Application',

    controllers: [
        'FlickrFeedrD.controller.RecordDetailContainerCtrl',
        'FlickrFeedrD.controller.RecordsListContainerCtrl'
    ]
});
