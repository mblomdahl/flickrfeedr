/**
 * @author Mats Blomdahl <mats.blomdahl@gmail.com>
 *
 * A `FlickrRecordModel` record represents a list item retrieved from the [Flickr Public Feed JSON API][1]. API output
 * example:
 *
 *     {
 *         title: "Different Bridges",
 *         link: "http://www.flickr.com/photos/40950306@N08/10125594754/",
 *         media: {
 *             m: "http://farm6.staticflickr.com/5538/10125594754_26f6bd1250_m.jpg"
 *         },
 *         date_taken: "2013-10-03T12:20:28-08:00",
 *         description: " <p><a href="http://www.flickr.com/people/40950306@N08/">ChristineW49</a> posted a photo:</p> <p><a href="http://www.flickr.com/photos/40950306@N08/10125594754/" title="Different Bridges"><img src="http://farm6.staticflickr.com/5538/10125594754_26f6bd1250_m.jpg" width="156" height="240" alt="Different Bridges" /></a></p> <p>A modern bridge with a shallower slope up</p>",
 *         published: "2013-10-06T21:21:20Z",
 *         author: "nobody@flickr.com (ChristineW49)",
 *         author_id: "40950306@N08",
 *         tags: "venice italy bridges"
 *     }
 *
 * [1]: http://api.flickr.com/services/feeds/photos_public.gne?format=json
 */
Ext.define('FlickrFeedrD.model.FlickrRecordModel', {
    extend: 'Ext.data.Model',

    fields: [
        {
            name: 'id',
            type: 'string'
        },
        {
            name: 'title',
            type: 'string',
            defaultValue: "[Undefined]"
        },
        {
            name: 'link',
            type: 'string',
            convert: function(v, rec) {
                if (v) {
                    // Sets the model `id` property to the `<author>/<image>/` component of the item link
                    rec.set('id', v.replace('http://www.flickr.com/photos/', ''));
                    return v;
                }
            }
        },
        {
            name: 'description',
            type: 'string'
        },
        {
            name: 'media',
            type: 'string',
            convert: function(v, rec) {
                // Replaces the API output `media` field with its single member `m` (i.e. the image URL)
                if (v && v.m) {
                    return v.m;
                }
            }
        },
        {
            name: 'date_taken',
            type: 'date',
            readDateFormat: 'c'
        },
        {
            name: 'published',
            type: 'date',
            dateFormat: 'c'
        },
        {
            name: 'author',
            type: 'string',
            convert: function(v, rec) {
                if (v) {
                    // Removes username wrapper, e.g. `nobody@flickr.com (ChristineW49)` -> `ChristineW49`
                    var result = /\((.+)\)$/ig.exec(v);
                    if (result) {
                        return result[result.length - 1];
                    }
                }
            }
        },
        {
            name: 'author_id',
            type: 'string',
            convert: function(v, rec) {
                if (v) {
                    // Wraps `author_id` to form author profile URL
                    return Ext.String.format('http://www.flickr.com/people/{0}/', v);
                }
            }
        },
        {
            name: 'tags',
            type: 'auto',
            convert: function(v, rec) {
                if (v) {
                    // Converts tag list to array, e.g. `"venice italy bridges"` -> `['venice', 'italy', 'bridges']`
                    return v.split(' ');
                }
            }
        }
    ]
});
