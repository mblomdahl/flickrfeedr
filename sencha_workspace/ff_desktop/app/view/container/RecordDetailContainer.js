/**
 * @author Mats Blomdahl <mats.blomdahl@gmail.com>
 *
 * Container rendering Flickr item details.
 */
Ext.define('FlickrFeedrD.view.container.RecordDetailContainer', {
    extend: 'Ext.container.Container',
    alias: 'widget.recorddetailcontainer',

    autoScroll: true,

    initComponent: function() {
        var me = this;

        var itemDetailTpl = new Ext.XTemplate(
            '<section class="flickr-record-detail-view">',
                '<tpl for=".">',
                    '<article class="flickr-record-item-detail">',
                        '<hgroup>',
                            '<button onclick="window.history.back();">Back</button>',
                            '<h1><a href="{link}">{title}</a></h1>',
                            '<h3>',
                                '<span>Published: <time>{published:date("Y-m-d H:i")}</time></span>',
                                '<tpl if="date_taken">',
                                    '<span class="pipe-separator"></span>',
                                    '<span>Taken: <time>{date_taken:date("Y-m-d H:i")}</time></span>',
                                '</tpl>',
                            '</h3>',
                        '</hgroup>',
                        '<aside class="flickr-record-item-detail-origin">',
                            '<a href="{link}">View on Flickr</a>',
                            '<span class="pipe-separator"></span>',
                            '<a href="{author_id}">{author}</a>',
                        '</aside>',
                        '<div>',
                            '<img src="{media}"/>',
                            '<div>',
                                '<div>',
                                    '{[this.formatDescription(values.description)]}',
                                '</div>',
                                '<aside class="flickr-record-item-detail-tags">',
                                    '<h2>Tags:</h2>',
                                    '<div>',
                                        '<tpl foreach="tags">',
                                            '<a href="#listTags/{.}">{.}</a> ',
                                        '</tpl>',
                                    '</div>',
                                '</aside>',
                            '</div>',
                        '</div>',
                    '</article>',
                '</tpl>',
            '</section>',
            {
                /**
                 * Extracts trailing `p` tags with item description.
                 *
                 * @param {String} description
                 * @returns {String} HTML markup of item description `p` tags (if such exists)
                 */
                formatDescription: function(description) {
                    var descElement = document.createElement('div');
                    descElement.innerHTML = description;
                    var pElements = descElement.getElementsByTagName('p');
                    var descHtml = Ext.toArray(pElements, 2); // Exclude `img` and `author` paragraphs
                    for (var i = 0, j = descHtml.length; i < j; i++) {
                        descHtml[i] = descHtml[i].outerHTML;
                    }
                    return descHtml.join('');
                }
            }
        );

        Ext.applyIf(me, {
            tpl: itemDetailTpl,
            items: [
                /*{
                    xtype: 'button',
                    text: 'Back',
                    handler: function(button, clickEvent, eOpts) {
                        Ext.util.History.back();
                    }
                }*/
            ]
        });

        me.callParent(arguments);
    }

});
