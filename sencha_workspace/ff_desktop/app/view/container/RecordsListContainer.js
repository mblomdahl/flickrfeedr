/**
 * @author Mats Blomdahl <mats.blomdahl@gmail.com>
 *
 * Container wrapping the search bar and Flickr items list.
 */
Ext.define('FlickrFeedrD.view.container.RecordsListContainer', {
    extend: 'Ext.container.Container',
    alias: 'widget.recordslistcontainer',

    requires: [
        'Ext.form.field.Text',
        'FlickrFeedrD.view.container.RecordDetailContainer'
    ],

    autoScroll: true,

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'textfield',
                    inputType: 'search',
                    fieldLabel: 'Search Tags',
                    allowBlank: false
                },
                {
                    xtype: 'recordslistdataview'
                }
            ]
        });

        me.callParent(arguments);
    }

});
