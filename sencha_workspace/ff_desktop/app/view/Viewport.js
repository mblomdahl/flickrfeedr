/**
 * @author Mats Blomdahl <mats.blomdahl@gmail.com>
 *
 * Application main viewport.
 */
Ext.define('FlickrFeedrD.view.Viewport', {
    extend: 'Ext.container.Viewport',

    requires: [
        'Ext.layout.container.Card',
        'FlickrFeedrD.view.view.RecordsListDataView',
        'FlickrFeedrD.view.container.RecordsListContainer'
    ],

    layout: {
        type: 'fit'
    },

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'container',
                    layout: 'card',
                    activeItem: 0,
                    items: [
                        {
                            xtype: 'recordslistcontainer'
                        },
                        {
                            xtype: 'recorddetailcontainer'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    },

    renderTo: Ext.getBody()

});
