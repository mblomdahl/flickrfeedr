/**
 * @author Mats Blomdahl <mats.blomdahl@gmail.com>
 *
 * DataView component rendering the `FlickrRecordsStore` records.
 */
Ext.define('FlickrFeedrD.view.view.RecordsListDataView', {
    extend: 'Ext.view.View',
    alias: 'widget.recordslistdataview',

    store: 'FlickrFeedrD.store.FlickrRecordsStore',
    itemSelector: 'article.flickr-record-list-item',
    emptyText: 'No matching Flickr posts found!',
    loadingText: 'Loading Flickr API ...',

    initComponent: function() {
        var me = this;

        var listItemTpl = new Ext.XTemplate(
            '<section class="flickr-records-list-view">',
                '<tpl for=".">',
                    '<article class="flickr-record-list-item">',
                        '<a href="#viewRecordDetail/{id}">',
                            '<img src="{media}"/>',
                        '</a>',
                        '<div><div><div>',
                            '<hgroup>',
                                '<h2><a href="#viewRecordDetail/{id}">{title}</a></h2>',
                                '<h3>',
                                    '<span>Published: <time>{[this.formatDate(values.published)]}</time></span>',
                                    '<span class="pipe-separator"></span>',
                                    '<tpl if="date_taken">',
                                        '<span>Taken: <time>{[this.formatDate(values.date_taken)]}</time></span>',
                                    '</tpl>',
                                '</h3>',
                            '</hgroup>',
                            '<aside class="flickr-record-list-origin">',
                                '<a href="{link}">View on Flickr</a>',
                                '<span class="pipe-separator"></span>',
                                '<a href="{author_id}">{author}</a>',
                            '</aside>',
                        '</div></div></div>',
                    '</article>',
                '</tpl>',
            '</section>',
            {
                // Member function
                formatDate: function(date) {
                    return Ext.Date.format(date, 'Y-m-d H:i');
                }
            }
        );

        Ext.applyIf(me, {
            tpl: listItemTpl,
            loadingHeight: window.innerHeight
        });

        me.callParent(arguments);
    }

});
