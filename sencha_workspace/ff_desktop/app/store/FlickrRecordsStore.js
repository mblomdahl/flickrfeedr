/**
 * @author Mats Blomdahl <mats.blomdahl@gmail.com>
 *
 * The `FlickrRecordsStore` contain list items retrieved from the [Flickr Public Feed JSON API][1]. API output example:
 *     jsonFlickrFeed({
 *         title: "Uploads from everyone",
 *         link: "http://www.flickr.com/photos/",
 *         description: "",
 *         modified: "2013-10-06T21:21:19Z",
 *         generator: "http://www.flickr.com/",
 *         items: [
 *             {
 *                 title: "Different Bridges",
 *                 link: "http://www.flickr.com/photos/40950306@N08/10125594754/",
 *                 media: {
 *                     m: "http://farm6.staticflickr.com/5538/10125594754_26f6bd1250_m.jpg"
 *                 },
 *                 date_taken: "2013-10-03T12:20:28-08:00",
 *                 description: " <p><a href="http://www.flickr.com/people/40950306@N08/">ChristineW49</a> posted a photo:</p> <p><a href="http://www.flickr.com/photos/40950306@N08/10125594754/" title="Different Bridges"><img src="http://farm6.staticflickr.com/5538/10125594754_26f6bd1250_m.jpg" width="156" height="240" alt="Different Bridges" /></a></p> <p>A modern bridge with a shallower slope up</p>",
 *                 published: "2013-10-06T21:21:20Z",
 *                 author: "nobody@flickr.com (ChristineW49)",
 *                 author_id: "40950306@N08",
 *                 tags: "venice italy bridges"
 *             },
 *             /.../
 *         ]
 *     });
 *
 * [1]: http://api.flickr.com/services/feeds/photos_public.gne?format=json
 */
Ext.define('FlickrFeedrD.store.FlickrRecordsStore', {
    extend: 'Ext.data.Store',

    requires: [
        'Ext.data.proxy.JsonP',
        'FlickrFeedrD.model.FlickrRecordModel'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'FlickrFeedrD.model.FlickrRecordModel',
            storeId: 'flickrRecords',
            proxy: {
                type: 'jsonp',
                url : '//api.flickr.com/services/feeds/photos_public.gne',
                callbackKey: 'jsoncallback',
                extraParams: {
                    format: 'json',
                    tagmode: 'all',
                    tags: undefined
                },
                reader: {
                    type: 'json',
                    idProperty: 'link',
                    root: 'items'
                }
            }
        }, cfg)]);
    }
});
