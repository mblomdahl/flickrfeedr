/**
 * @author Mats Blomdahl <mats.blomdahl@gmail.com>
 *
 * The `RecordsListContainerCtrl` controls the Flickr items list view and search box.
 */
Ext.define('FlickrFeedrD.controller.RecordsListContainerCtrl', {
    extend: 'Ext.app.Controller',
    alias: 'controller.recordslistcontainerctrl',

    stores: [
        'FlickrFeedrD.store.FlickrRecordsStore'
    ],

    views: [
        'FlickrFeedrD.view.Viewport',
        'FlickrFeedrD.view.container.RecordsListContainer',
        'FlickrFeedrD.view.view.RecordsListDataView'
    ],

    refs: [
        /*{
            ref: 'viewportContainer',
            selector: 'viewport > container',
            xtype: 'container'
        },*/
        {
            ref: 'recordsListContainer',
            selector: 'recordslistcontainer',
            xtype: 'recordslistcontainer'
        },
        /*{
            ref: 'recordsListDataView',
            selector: 'recordslistcontainer > recordslistdataview',
            xtype: 'recordslistdataview'
        },*/
        {
            ref: 'searchField',
            selector: 'recordslistcontainer > textfield',
            xtype: 'textfield'
        }
    ],

    dispatchTokens: [
        'listTags'
    ],

    init: function(application) {
        this.control({
            'recordslistcontainer > textfield': {
                specialkey: this.onSearchFieldSpecialKey
            }
        });
        this.listen({
            global: {
                exTokenChange: this.onExTokenChange
            }
        });
    },

    doSetActiveItem: function(container) {
        container = container || this.getRecordsListContainer();
        container.up('container').getLayout().setActiveItem(container);
    },

    doRefreshHashToken: function(tags) {
        tags = tags || 'potato';
        window.location.hash = Ext.String.format('listTags/{0}', tags);
    },

    doUpdateStore: function(tags) {
        var store = Ext.getStore('FlickrFeedrD.store.FlickrRecordsStore');
        if (store.proxy.extraParams.tags !== tags) {
            store.proxy.setExtraParam('tags', tags.split(' ').join(','));
            store.load();
            this.doRefreshHashToken(tags);
            return true;
        } else {
            return false;
        }
    },

    onLaunch: function(application) {
        this.doRefreshHashToken();
    },

    onSearchFieldSpecialKey: function(textfield, keyEvent, eOpts) {
        if (textfield.isValid() && keyEvent.getKey() === keyEvent.ENTER) {
            var inputTags = textfield.getValue();
            while (inputTags.search('  ') !== -1) {
                inputTags = inputTags.replace('  ', ' ');
            }
            this.doRefreshHashToken(inputTags.split(' ').join(','));
        }
    },

    onExTokenChange: function(newToken, oldToken, eOpts) {
        if (newToken.token && this.dispatchTokens.indexOf(newToken.token) !== -1) {
            this[newToken.token](newToken.argument);
        }
    },

    listTags: function(tags) {
        this.doUpdateStore(tags);
        this.doSetActiveItem();
    }

});
