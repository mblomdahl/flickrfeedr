/**
 * @author Mats Blomdahl <mats.blomdahl@gmail.com>
 *
 * The `RecordDetailContainerCtrl` controls the Flickr item details view.
 */
Ext.define('FlickrFeedrD.controller.RecordDetailContainerCtrl', {
    extend: 'Ext.app.Controller',
    alias: 'controller.recorddetailcontainerctrl',

    stores: [
        'FlickrFeedrD.store.FlickrRecordsStore'
    ],

    views: [
        'FlickrFeedrD.view.Viewport',
        'FlickrFeedrD.view.container.RecordDetailContainer'
    ],

    refs: [
        {
            ref: 'viewportContainer',
            selector: 'viewport > container',
            xtype: 'container'
        },
        {
            ref: 'recordDetailContainer',
            selector: 'recorddetailcontainer',
            xtype: 'recorddetailcontainer'
        }
    ],

    dispatchTokens: [
        'viewRecordDetail'
    ],

    init: function(application) {
        this.control({
            'recorddetailcontainer': {
                deactivate: this.onContainerDeactivate
            }
        });
        this.listen({
            global: {
                exTokenChange: this.onExTokenChange
            }
        });
    },

    doSetActiveItem: function(container) {
        container = container || this.getRecordDetailContainer();
        container.up('container').getLayout().setActiveItem(container);
    },

    doUpdateDetailView: function(container, recordId) {
        container = container || this.getRecordDetailContainer();
        var recordData = {};
        if (recordId) {
            var store = Ext.getStore('FlickrFeedrD.store.FlickrRecordsStore');
            recordData = store.getById(recordId).getData();
        }
        container.update(recordData);
    },

    onContainerDeactivate: function(container, eOpts) {
        this.doUpdateDetailView(container, undefined);
    },

    onExTokenChange: function(newToken, oldToken, eOpts) {
        if (newToken.token && this.dispatchTokens.indexOf(newToken.token) !== -1) {
            this[newToken.token](newToken.argument);
        }
    },

    viewRecordDetail: function(recordId) {
        this.doUpdateDetailView(undefined, recordId);
        this.doSetActiveItem();
    }

});
