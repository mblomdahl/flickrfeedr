# ext-default/sass

This folder contains SASS files of various kinds, organized in sub-folders:

    ext-default/sass/etc
    ext-default/sass/src
    ext-default/sass/var
